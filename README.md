# SMILE Testing Docker Image

SMILE runs various tests against it's output. This Docker image has all of them already installed. It's based on Alpine Linux to keep the image as small as possible.

## Pipeline usage

This URL for this Docker image is:

```
registry.gitlab.com/wearesmile/operations/infrastructure/docker-containers/test:latest
```