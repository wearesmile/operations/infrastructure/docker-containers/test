FROM node:12-alpine

RUN apk add --update php7 php7-json php7-phar php7-xmlwriter php7-iconv php7-tokenizer php7-simplexml php7-mbstring php7-openssl php7-zlib php7-xml php7-dom

RUN apk add --update git bash curl

RUN wget https://develop.svn.wordpress.org/trunk/.jshintrc

RUN npm install -g eslint \
    npm install -g https://github.com/WordPress-Coding-Standards/eslint-plugin-wordpress

RUN npm install stylelint --save-dev -g \
    npm install stylelint-config-wordpress --save-dev -g

RUN cd /root/ && \
    curl -L https://getcomposer.org/installer -o composer-setup.php && \
    php composer-setup.php && \
    rm  composer-setup.php && \
    mv composer.phar /usr/local/bin/composer && \
    chmod +rx /usr/local/bin/composer && \
    composer create-project wp-coding-standards/wpcs --no-dev

ENV PATH=$PATH:/root/wpcs/vendor/bin/
